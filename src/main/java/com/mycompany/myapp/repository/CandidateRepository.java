package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Candidate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CandidateRepository extends PagingAndSortingRepository<Candidate, Integer> {

}
