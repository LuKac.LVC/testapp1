package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Candidate;
import com.mycompany.myapp.repository.CandidateRepository;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import com.mycompany.myapp.service.exception.AccessForbiddenException;
import com.mycompany.myapp.service.exception.BadRequestException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

@Service
public class CandidateService {

    @Autowired
    private CandidateRepository candidateRepository;
    @Autowired
    private  UserACL userACL;
    /**
        import Candidate manually
     */
    public Candidate importCandidateManually(Candidate candidate) {
        if (!userACL.isAdmin()) {
            throw new AccessForbiddenException("error.notAdmin");
        }
        if (candidate.getFirstName() == null || candidate.getLastName() == null) {
            throw new BadRequestException("error.nullValue", null);
        }
        if (!candidate.getBirthYear().matches("[12][0-9]{3}")) {
            throw new BadRequestException("error.invalidYear", null);
        }
        if (!candidate.getPersonalNumber().matches("^\\d{3}-\\d{3}-\\d{3}-\\d{3}$")){
            throw new BadRequestException("error.invalidPersonalNumber", null);
        }
        if (!candidate.getPhoneNumber().matches("\\d+")){
            throw new BadRequestException("error.invalidPhone", null);
        }
        if (!candidate.getEmailPrivate().matches("[a-zA-Z0-9]+[._a-zA-Z0-9!#$%&'*+-/=?^_`{|}~]*[a-zA-Z]*@[a-zA-Z0-9]{2,8}.[a-zA-Z.]{2,6}") || !candidate.getEmailWork().matches("[a-zA-Z0-9]+[._a-zA-Z0-9!#$%&'*+-/=?^_`{|}~]*[a-zA-Z]*@[a-zA-Z0-9]{2,8}.[a-zA-Z.]{2,6}")){
            throw new BadRequestException("error.invalidEmail", null);
        }
        candidateRepository.save(candidate);
        return candidate;
    }

}
